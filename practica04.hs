module Prac04 where

data BT a = E | NodeB (BT a) a (BT a) deriving Show

completo :: a -> Int -> BT a
completo _ 0 = E
completo a n = NodeB tr a tr where tr = completo a ((-) n 1) 

balanceado :: a -> Int -> BT a
balanceado _ 0 = E
balanceado a 1 = NodeB E a E
balanceado a n = if even ((+) n 1) then (NodeB tr a tr)  
                    else (NodeB tr a (balanceado a ((div (n-1) 2) + 1)))
                where tr = balanceado a (div (n-1) 2)